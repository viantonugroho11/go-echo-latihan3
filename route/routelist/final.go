package routelist

import (
	// "code-echo/api/peremajaan"

	"code-echo/api/peremajaan"

	"github.com/labstack/echo/v4"
)
func FinalSetting(echo *echo.Group){

	echo.POST("/add", peremajaan.SimpanFinalSetting)
	echo.POST("/update", peremajaan.UpdateFinalSetting)
	echo.GET("/get", peremajaan.GetFinalSetting)
}