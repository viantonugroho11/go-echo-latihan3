package peremajaan
import
(
	"code-echo/db"
	"code-echo/model"
	"github.com/labstack/echo/v4"
	uuid "github.com/satori/go.uuid"
	"net/http"
)

func SimpanFinalSetting(c echo.Context) error {
	//Inisiasi Model Baru dari Struct Even Instansi
	ModelFinalSetting := new(model.Final_Setting)

	//Memperoleh isi input / parameter yang di kirim dari client
	if err := c.Bind(ModelFinalSetting); err != nil {
		return err
	}

	//Inisiasi Koneksi Database
	DbConn := db.Manager()

	//Generate UUID Untuk Id record yang baru
	u1 := uuid.Must(uuid.NewV4())
	Id := u1.String()

	//Memberi Nilai Id yang ada dalam ModelEventInstansi
	ModelFinalSetting.Id = Id

	// Insert Di tabel usulan
	 dbc := DbConn.Debug().Create(&ModelFinalSetting);

	 //Kondisi Jika Terjadi error saat eksekusi query
	 if dbc.Error != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]string{
			"error":   "true",
			"message": dbc.Error.Error(),
		})
	}

	// Mereturn Response dalam format JSON
	return c.JSON(http.StatusOK, map[string]string{
		"error":   "false",
		"message":  "Berhasil Disimpan",
		"Id": Id,
	})
}

func UpdateFinalSetting(c echo.Context) error {
	//Inisiasi Model Baru dari Struct Even Instansi
	ModelFinalSetting := new(model.Final_Setting)

	//Memperoleh isi input / parameter yang di kirim dari client
	if err := c.Bind(ModelFinalSetting); err != nil {
		return err
	}

	//Inisiasi Koneksi Database
	DbConn := db.Manager()
	// Insert Di tabel usulan
	dbc := DbConn.Debug().Model(&ModelFinalSetting).Where("id = ?", ModelFinalSetting.Id).Update(ModelFinalSetting);

	//Kondisi Jika Terjadi error saat eksekusi query
	if dbc.Error != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]string{
			"error":   "true",
			"message": dbc.Error.Error(),
		})
	}

	// Mereturn Response dalam format JSON
	return c.JSON(http.StatusOK, map[string]string{
		"error":   "false",
		"message": "Berhasil Diupdate",
	})

}

func GetFinalSetting(c echo.Context) error {
	DbCon := db.Manager()
	Final := []model.Final_Setting{}
	DbCon.Find(&Final)
	// for i := range Final{
	// 	if Final[i].Periode == 0 {
	// 		Final[i].StatusPeriode = "Periode Nonaktif"
	// 	}
	// }
	return c.JSON(http.StatusOK, Final)
}